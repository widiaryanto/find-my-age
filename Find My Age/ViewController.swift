//
//  ViewController.swift
//  Find My Age
//
//  Created by Muhamad Widi Aryanto on 15/08/19.
//  Copyright © 2019 Widi Aryanto. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var ageLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func calculateMyAge(_ sender: Any) {
        // get selected date from date picker
        let birthDate = self.datePicker.date // yyyy-MM-dd HH:mm:ss +0000
        // get current date
        let today = Date()
        // check our birth date is earlier then today
        if birthDate >= today {
            let alertController = UIAlertController(title: "Error", message: "Please enter a valid date", preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "Yes", style: .default, handler: nil)
            alertController.addAction(alertAction)
            self.present(alertController, animated: true, completion: nil)
            return
        }
        // create instance current calender
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day], from: birthDate, to: today)
        
        guard let ageYears = components.year else {return}
        guard let ageMonth = components.month else {return}
        guard let ageDay = components.day else {return}
        
        self.ageLabel.text = "\(ageYears) years, \(ageMonth) month, \(ageDay) day"
        
    }
    
}

